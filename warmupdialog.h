#ifndef WARMUPDIALOG_H
#define WARMUPDIALOG_H

#include <QDialog>
#include "warmupthread.h"

namespace Ui {
class WarmUpDialog;
}

class WarmUpDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WarmUpDialog(QWidget *parent = 0);
    ~WarmUpDialog();

public:
    void closeWarmup();

private:
    Ui::WarmUpDialog *ui;
    QThread* threadWarmup;
    warmupThread* Warmupworker;


private slots:
    void setValue(double value);
    void setTime();

    void on_stopButton_clicked();

    void on_spinBoxCH1_valueChanged(double arg1);

    void on_spinBoxCH2_valueChanged(double arg1);

signals:
    stopThread();
    refreshTime();

};

#endif // WARMUPDIALOG_H

