#ifndef HVCOM_H
#define HVCOM_H

#include <QObject>
#include <QtSerialPort/QSerialPort>


class hvCom : public QObject {
    Q_OBJECT

public:
    hvCom();
    ~hvCom();

public slots:
    void run();
    void stop();
signals:
    void finished();
public:

private:
    void checkPressure();
private:
    QSerialPort *serial;
    QByteArray data;
    QString checkChar;

};

#endif // HVCOM_H
