#ifndef G2COM_H
#define G2COM_H

#include <QObject>
#include <QtSerialPort/QSerialPort>

class G2Com : public QObject {
    Q_OBJECT

public:
    G2Com();
    ~G2Com();

public slots:
    void run();
    void stop();
signals:
    void finished();
public:


private:
    QSerialPort *serial;
    QByteArray data;
    QString checkChar;



};

#endif // G2COM_H
