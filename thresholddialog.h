#ifndef THRESHOLDDIALOG_H
#define THRESHOLDDIALOG_H

#include <QDialog>

namespace Ui {
class thresholdDialog;
}

class thresholdDialog : public QDialog
{
    Q_OBJECT

public:
    explicit thresholdDialog(QWidget *parent = 0);
    ~thresholdDialog();

private slots:
    void on_spinBox_valueChanged(double arg1);

private:
    Ui::thresholdDialog *ui;
};

#endif // THRESHOLDDIALOG_H
