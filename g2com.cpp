#include "g2com.h"
#include "global.h"
#include <QDebug>

extern struct Settings G2currentSettings;
extern struct Gauge g2;
extern struct Gauge g3;


G2Com::G2Com()
{

}

G2Com::~G2Com()
{

}

void G2Com::run()
{
    checkChar = "";

    serial = new QSerialPort(this);
    serial->setPortName(G2currentSettings.name);
    serial->setBaudRate(G2currentSettings.baudRate);
    serial->setDataBits(G2currentSettings.dataBits);
    serial->setParity(G2currentSettings.parity);
    serial->setStopBits(G2currentSettings.stopBits);
    serial->setFlowControl(G2currentSettings.flowControl);
    if (serial->open(QIODevice::ReadWrite)) {
        serial->write("\x03");
        serial->clear();
        serial->write("COM,2\n");
        serial->waitForBytesWritten(100);
        if(serial->waitForReadyRead(500)){
            data = serial->readAll();
            qDebug()<<"G2 Receive:"<<data;
        }
        if (QString::fromUtf8(data.data()) == checkChar) {
            G2currentSettings.status = true;
            qDebug()<<"G2: Connected!";
            serial->write("\x03");
            serial->clear();
            serial->write("COM,1\n");
            //find the begining
            int n = 1;
            while(n)
            {
                serial->waitForReadyRead(100);
                QByteArray test = serial->readAll();
                if (QString::fromUtf8(test.data()) == "\n") {
                    n=0;
                }
            }
            while(1)
            {
                if(serial->bytesAvailable() == 29) {
                    data = serial->readAll();
                    //qDebug()<<"G2 Receive:"<<data<<endl;
                    g2.status = data.mid(3,1).toInt();
                    g2.value = data.mid(5,11).toDouble();
                    //qDebug()<<data.mid(3,1)<<data.mid(5,11)<<endl;
                    //qDebug()<<g2.status<<g2.value;
                    g3.status = data.mid(17,1).toInt();
                    g3.value = (data.mid(20,11).append(data.mid(0,1))).toDouble();
                    //qDebug()<<data.mid(17,1)<<data.mid(20,11).append(data.mid(0,1))<<endl;
                    //qDebug()<<g3.status<<g3.value;

                } else {
                    serial->waitForReadyRead(10);
                }
            }



        } else {
            G2currentSettings.status = false;
            qDebug()<<"G2: Wrong connection!";
            serial->close();
            emit finished();
        }
    } else {
        qDebug()<<"G2: Connection failed!";
    }





}

void G2Com::stop()
{
    qDebug()<<"G2 shut down!";
    if (serial->isOpen()) {
        serial->close();
    }
    emit finished();
}
