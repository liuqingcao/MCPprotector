#include "warmupdialog.h"
#include "ui_warmupdialog.h"
#include "global.h"
#include "windows.h"
#include "warmupthread.h"
#include <QThread>
#include <QDebug>

extern struct System sys;
extern struct HVinfo ch1;
extern struct HVinfo ch2;
extern struct System sys;

WarmUpDialog::WarmUpDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WarmUpDialog)
{
    ui->setupUi(this);

    ui->warmSpeed->setSingleSteps(1);
    ui->warmSpeed->setUpperBound(50);
    ui->warmSpeed->setLowerBound(0);
    ui->warmSpeed->setValue(sys.warmupSpeed);

    ui->spinBoxCH1->setRange(0,2);
    ui->spinBoxCH1->setSingleStep(0.1);
    ui->spinBoxCH1->setValue(double(sys.warmupCh1)/1000);

    ui->spinBoxCH2->setRange(0,4);
    ui->spinBoxCH2->setSingleStep(0.2);
    ui->spinBoxCH2->setValue(double(sys.warmupCh2)/1000);

    connect(ui->warmSpeed,SIGNAL(valueChanged(double)),this,SLOT(setValue(double)));

    connect(this, SIGNAL(refreshTime()),this,SLOT(setTime()));
    emit refreshTime();

    QThread* threadWarmup = new QThread;
    warmupThread* Warmupworker = new warmupThread();
    Warmupworker->moveToThread(threadWarmup);
    connect(Warmupworker, SIGNAL(finished()), threadWarmup, SLOT(quit()));
    connect(Warmupworker, SIGNAL(finished()), Warmupworker, SLOT(deleteLater()));
    connect(threadWarmup, SIGNAL(finished()), threadWarmup, SLOT(deleteLater()));
    connect(Warmupworker, SIGNAL(process(int)), ui->warmProgress, SLOT(setValue(int)));
    connect(Warmupworker, SIGNAL(processRange(int,int)), ui->warmProgress, SLOT(setRange(int,int)));
    threadWarmup->start();
    connect(this, SIGNAL(stopThread()), Warmupworker, SLOT(destoryThread()));
    connect(ui->applyButton, SIGNAL(clicked()),Warmupworker, SLOT(turnOn()));
    connect(ui->shutdownButton, SIGNAL(clicked()),Warmupworker, SLOT(shutDown()));

}

WarmUpDialog::~WarmUpDialog()
{
    delete ui;
}



void WarmUpDialog::setValue(double value)
{

    sys.warmupSpeed = value + 1;
    emit refreshTime();

}


void WarmUpDialog::setTime()
{

    if (sys.warmup) {
        ui->warmTimeLCD->display(int(ch1.readV/sys.warmupSpeed));
    } else {
        ui->warmTimeLCD->display(int(sys.warmupCh1/sys.warmupSpeed));
    }

}


void WarmUpDialog::closeWarmup()
{
    emit stopThread();
}



void WarmUpDialog::on_stopButton_clicked()
{
    sys.warmupRunning = false;
    sys.warmup = false;
    ch1.setV = ch1.readV;
    ch1.oldV = ch1.setV;
    ch1.oldI = ch1.setI;
    ch2.setV = ch2.readV;
    ch2.oldV = ch2.setV;
    ch2.oldI = ch2.setI;
    emit refreshTime();
}

void WarmUpDialog::on_spinBoxCH1_valueChanged(double arg1)
{
    sys.warmupCh1 = arg1 * 1000;
    qDebug()<<"Set Ch1 warm up voltage to"<<sys.warmupCh1;
}

void WarmUpDialog::on_spinBoxCH2_valueChanged(double arg1)
{
    sys.warmupCh2 = arg1 * 1000;
    qDebug()<<"Set Ch2 warm up voltage to"<<sys.warmupCh2;
}
