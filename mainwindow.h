#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore/QtGlobal>
#include "settingsdialog.h"
#include "thresholddialog.h"
#include "warmupdialog.h"
#include <qwt_dial.h>
#include <qwt_thermo.h>

namespace Ui {
class MainWindow;
}

QT_END_NAMESPACE

class SettingsDialog;
class thresholdDialog;
class WarmUpDialog;


class MainWindow : public QMainWindow
{
    Q_OBJECT

protected:
    void closeEvent(QCloseEvent *event);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    SettingsDialog *settings;
    thresholdDialog *threshold;
    WarmUpDialog *warmup;
    QString statusColors[10];
    bool turnRed;
private:
    void setupDial(QwtDial *dial);
    void setupTher(QwtThermo *ther);
    void setToZero();


private slots:
    void about();
    void update();

    void on_spinBoxV1_valueChanged(double arg1);
    void on_spinBoxI1_valueChanged(double arg1);
    void on_spinBoxV2_valueChanged(double arg1);
    void on_spinBoxI2_valueChanged(double arg1);
};

#endif // MAINWINDOW_H
