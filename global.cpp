#include "global.h"

extern struct System sys;
extern struct Gauge g1;
extern struct Gauge g2;
extern struct Gauge g3;
extern struct HVinfo ch1;
extern struct HVinfo ch2;

void initial()
{
    sys.threshold = 1e-5;
    sys.warmup = false;
    sys.warmupRunning = true;
    sys.warmupSpeed = 15;
    sys.warmupCh1 = 1500;
    sys.warmupCh2 = 3500;
    sys.safe = true;
    sys.first = true;

    ch1.oldI = 0.0;
    ch1.oldV = 0;
    ch1.readI = 0.0;
    ch1.readV = 0.0;
    ch1.setI = 0.0;
    ch1.setV = 0;
    ch2.oldI = 0.0;
    ch2.oldV = 0;
    ch2.readI = 0.0;
    ch2.readV = 0.0;
    ch2.setI = 0.0;
    ch2.setV = 0;

    g1.status = 2;
    g1.value = 0.0;
    g2.status = 2;
    g2.value = 0.0;
    g3.status = 2;
    g3.value = 0.0;
}
