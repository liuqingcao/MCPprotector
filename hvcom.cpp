#include "hvcom.h"
#include "global.h"
#include <QDebug>
#include <QMessageBox>


#define PROTECT_ON

extern struct Settings HVcurrentSettings;
extern struct Gauge g1;
extern struct Gauge g2;
extern struct Gauge g3;
extern struct HVinfo ch1;
extern struct HVinfo ch2;
extern struct System sys;


hvCom::hvCom()
{

}

hvCom::~hvCom()
{

}



void hvCom::run()
{
    checkChar = "6";

    serial = new QSerialPort(this);
    serial->setPortName(HVcurrentSettings.name);
    serial->setBaudRate(HVcurrentSettings.baudRate);
    serial->setDataBits(HVcurrentSettings.dataBits);
    serial->setParity(HVcurrentSettings.parity);
    serial->setStopBits(HVcurrentSettings.stopBits);
    serial->setFlowControl(HVcurrentSettings.flowControl);
    if (serial->open(QIODevice::ReadWrite)) {
        serial->clear();
        serial->write("E1=1\r\n");
        serial->write("E2=1\r\n");
        serial->write("A1=1\r\n");
        serial->write("A2=1\r\n");
        serial->write("P1=+\r\n");
        serial->write("P2=+\r\n");
        serial->clear();
        serial->write("#1\r\n");
        serial->clear();
        serial->write("#1\r\n");
        serial->waitForBytesWritten(100);
        serial->waitForReadyRead(100);
        data = serial->readAll();
        qDebug()<<"HV Receive:"<<data;
        if (QString::fromUtf8(data.data()).contains(checkChar)) {
            HVcurrentSettings.status = true;
            qDebug()<<"HV: Connected!";
            while(true) {
                //check the gas pressure
#ifdef PROTECT_ON
                checkPressure();
#endif
            while(sys.safe) {
#ifdef PROTECT_ON
                checkPressure();
#endif
                //set U1
                if (ch1.oldV != ch1.setV) {
                    serial->clear();
                    QString Write = QString("D1=%1").arg(ch1.setV) + QString("\r\n");
                    ch1.oldV = ch1.setV;
                    qDebug()<<"HV-U1 Send:"<<Write.toLatin1();
                    serial->write(Write.toLatin1());
                    serial->waitForBytesWritten(100);
                    serial->waitForReadyRead(100);
                    data = serial->readAll();
                }
                //get U1
                if (1) {
                    serial->clear();
                    QString Write = "U1\r\n";
                    //qDebug()<<"HV-U1 Send:"<<Write.toLatin1();
                    serial->write(Write.toLatin1());
                    serial->waitForBytesWritten(100);
                    int i = 1;
                    while (i<10) {
                        if (i == 1) {
                            data = serial->readAll();
                        } else {
                            data = data.append(serial->readAll());
                        }
                        serial->waitForReadyRead(10);
                        i++;
                    }
                }
                ch1.readV = data.mid(4,data.size()-6).toDouble();
                if (!sys.warmup) {
                    ch1.oldV = ch1.readV;
                    ch1.setV = ch1.oldV;
                }
                //qDebug()<<data.mid(4,data.size()-6)<<ch1.readV;
                //set I1
                if (ch1.oldI != ch1.setI) {
                    serial->clear();
                    QString Write = QString("C1=%1").arg(ch1.setI) + QString("\r\n");
                    ch1.oldI = ch1.setI;
                    qDebug()<<"HV-I1 Send:"<<Write.toLatin1();
                    serial->write(Write.toLatin1());
                    serial->waitForBytesWritten(100);
                    serial->waitForReadyRead(100);
                    data = serial->readAll();
                }
                //get I1
                if (1) {
                    serial->clear();
                    QString Write = "I1\r\n";
                   // qDebug()<<"HV-I1 Send:"<<Write.toLatin1();
                    serial->write(Write.toLatin1());
                    serial->waitForBytesWritten(100);
                    int i=1;
                    while (i<10) {
                        if (i == 1) {
                            data = serial->readAll();
                        } else {
                            data = data.append(serial->readAll());
                        }
                        serial->waitForReadyRead(10);
                        i++;
                    }
                }
                ch1.readI = data.mid(4,data.size()-6).toDouble();
                ch1.setI = ch1.readI;
                ch1.oldI = ch1.readI;
                //qDebug()<<data<<data.mid(4,data.size()-6)<<ch1.readI;

                //set U2
                if (ch2.oldV != ch2.setV) {
                    serial->clear();
                    QString Write = QString("D2=%1").arg(ch2.setV) + QString("\r\n");
                    ch2.oldV = ch2.setV;
                    qDebug()<<"HV-U2 Send:"<<Write.toLatin1();
                    serial->write(Write.toLatin1());
                    serial->waitForBytesWritten(100);
                    serial->waitForReadyRead(100);
                    data = serial->readAll();
                }
                //get U2
                if (1) {
                    serial->clear();
                    QString Write = "U2\r\n";
                    //qDebug()<<"HV-U2 Send:"<<Write.toLatin1();
                    serial->write(Write.toLatin1());
                    serial->waitForBytesWritten(100);
                    int i=1;
                    while (i<10) {
                        if (i == 1) {
                            data = serial->readAll();
                        } else {
                            data = data.append(serial->readAll());
                        }
                        serial->waitForReadyRead(10);
                        i++;
                    }
                }
                ch2.readV = data.mid(4,data.size()-6).toDouble();
                if (!sys.warmup) {
                    ch2.oldV = ch2.readV;
                    ch2.setV = ch2.oldV;
                }
                //qDebug()<<data.mid(4,data.size()-6)<<ch2.readV;
                //set I2
                if (ch2.oldI != ch2.setI) {
                    serial->clear();
                    QString Write = QString("C2=%1").arg(ch2.setI) + QString("\r\n");
                    ch2.oldI = ch2.setI;
                    qDebug()<<"HV-I2 Send:"<<Write.toLatin1();
                    serial->write(Write.toLatin1());
                    serial->waitForBytesWritten(100);
                    serial->waitForReadyRead(100);
                    data = serial->readAll();
                }
                //get I2
                if (1) {
                    serial->clear();
                    QString Write = "I2\r\n";
                    //qDebug()<<"HV-I2 Send:"<<Write.toLatin1();
                    serial->write(Write.toLatin1());
                    serial->waitForBytesWritten(100);
                    int i=1;
                    while (i<10) {
                        if (i == 1) {
                            data = serial->readAll();
                        } else {
                            data = data.append(serial->readAll());
                        }
                        serial->waitForReadyRead(10);
                        i++;
                    }
                }
                ch2.readI = data.mid(4,data.size()-6).toDouble();
                ch2.setI = ch2.readI;
                ch2.oldI = ch2.readI;
                //qDebug()<<data<<data.mid(4,data.size()-6)<<ch2.readI;
            }
            }
        } else {
            HVcurrentSettings.status = false;
            qDebug()<<"HV: Wrong connection!";
            serial->close();
            emit finished();
        }
    } else {
        qDebug()<<"HV: Connection failed!";
    }

}

void hvCom::stop()
{
    qDebug()<<"HV shut down!";
    if (serial->isOpen()) {
        serial->clear();
    }
    emit finished();
}


void hvCom::checkPressure()
{
    if (sys.warmup & (std::min(std::min(g1.value,g2.value),g3.value) > sys.threshold)) {

        sys.warmup = false;
        sys.warmupRunning = false;
        ch1.readI = 0.0;
        ch1.readV = 0.0;
        ch1.setI = 0.0;
        ch1.setV = 0.0;
        ch2.readI = 0.0;
        ch2.readV = 0.0;
        ch2.setI = 0.0;
        ch2.setV = 0.0;

        QString Write;
        serial->clear();
        Write = QString("D1=%1").arg(ch1.setV) + QString("\r\n");
        serial->write(Write.toLatin1());
        Write = QString("D2=%1").arg(ch2.setV) + QString("\r\n");
        serial->write(Write.toLatin1());

        if (sys.safe) {
            qDebug()<<"Over load!!!";
        }
        sys.safe = false;
    } else {
        if (!sys.safe) {
            qDebug()<<"Back to work!";
        }
        sys.safe = true;
    }
}
