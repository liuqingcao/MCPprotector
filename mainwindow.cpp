#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "QTimer"
#include "global.h"
#include <qpainter.h>
#include <qwt_dial_needle.h>
#include <qwt_round_scale_draw.h>
#include <QDebug>
#include <windows.h>
#include "warmupdialog.h"
#include <QCloseEvent>

System sys;
Gauge g1;
Gauge g2;
Gauge g3;
HVinfo ch1;
HVinfo ch2;
Settings HVcurrentSettings;
Settings G1currentSettings;
Settings G2currentSettings;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    initial();

    turnRed = false;

    statusColors[0] = "* { background-color: rgb(51,151,255) }";
    statusColors[1] = "* { background-color: rgb(0,0,0) }";
    statusColors[2] = "* { background-color: rgb(251,236,55) }";
    statusColors[3] = "* { background-color: rgb(20,192,44) }";
    statusColors[4] = "* { background-color: rgb(51,51,255) }";
    statusColors[5] = "* { background-color: rgb(151,51,255) }";
    statusColors[6] = "* { background-color: rgb(255,0,0) }";
    statusColors[9] = "* { background-color: rgb(170,170,127) }";


    ui->setupUi(this);
    //this->setStyleSheet(" background-color: rgb(236,233,216) ");



    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(200);


    //RS232 settings
    settings = new SettingsDialog;
    connect(ui->actionCommunication, SIGNAL(triggered()), settings, SLOT(show()));


    //pressure threshold
    threshold = new thresholdDialog;
    connect(ui->actionPressure_threshold, SIGNAL(triggered()),threshold, SLOT(show()));


    //MCP warm up
    warmup = new WarmUpDialog;
    connect(ui->actionMCP_Warm_up, SIGNAL(triggered()), warmup, SLOT(show()));

    //dial Voltage
    setupDial(ui->diV1);
    setupDial(ui->diV2);
    ui->diV2->setScale( 0.0, 4.0 );
    ui->diV2->setScaleStepSize( 0.4 );

    //progress Current
    setupTher(ui->thI1);
    setupTher(ui->thI2);

    //setting range
    ui->spinBoxV1->setRange(0,2);
    ui->spinBoxV1->setDecimals(3);
    ui->spinBoxV1->setSingleStep(0.01);
    ui->spinBoxV2->setRange(0,4);
    ui->spinBoxV2->setDecimals(3);
    ui->spinBoxV2->setSingleStep(0.01);
    ui->spinBoxI1->setRange(0,20);
    ui->spinBoxI1->setDecimals(4);
    ui->spinBoxI1->setSingleStep(0.1);
    ui->spinBoxI2->setRange(0,20);
    ui->spinBoxI2->setDecimals(4);
    ui->spinBoxI2->setSingleStep(0.1);
    //about
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->actionAboutQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    //

}

MainWindow::~MainWindow()
{

}


void MainWindow::closeEvent(QCloseEvent *event)
{
    //decrease the high voltage
    if (sys.warmup) {
//    if (sys.warmup |  (ch1.readV > 10) | (ch2.readV > 10)) {
        warmup->show();
        event->ignore();
    } else {
        warmup->closeWarmup();
        settings->closeSerialPort();
        Sleep(1000);
        delete warmup;
        delete settings;
        delete threshold;
        delete ui;
    }
}


void MainWindow::update()
{
    //
    if (sys.warmup){
        ui->spinBoxV1->setEnabled(true);
        ui->spinBoxV2->setEnabled(true);
        ui->spinBoxI1->setEnabled(true);
        ui->spinBoxI2->setEnabled(true);
        if (sys.first) {
            sys.first = false;
            ui->spinBoxV1->setValue(ch1.readV/1000);
            ui->spinBoxI1->setValue(ch1.readI*1000);
            ui->spinBoxV2->setValue(ch2.readV/1000);
            ui->spinBoxI2->setValue(ch2.readI*1000);
        }
    } else {
        ui->spinBoxV1->setEnabled(false);
        ui->spinBoxV2->setEnabled(false);
        ui->spinBoxI1->setEnabled(false);
        ui->spinBoxI2->setEnabled(false);
    }

    //
    setToZero();
    //update gauge 1
    ui->lcdP1->display(QString::number(g1.value,'e',1));
    if(g1.value >= 1e3) {
        ui->lcdP1->setStyleSheet(statusColors[9]);
    } else {
        ui->lcdP1->setStyleSheet(statusColors[g1.status]);
    }
    //update gauge 2
    ui->lcdP2->display(QString::number(g2.value,'e',1));
    if(g2.value >= 1e3) {
        ui->lcdP2->setStyleSheet(statusColors[9]);
    } else {
        ui->lcdP2->setStyleSheet(statusColors[g2.status]);
    }
    //update gauge 3
    ui->lcdP3->display(QString::number(g3.value,'e',1));
    if(g3.value >= 1e3) {
        ui->lcdP3->setStyleSheet(statusColors[9]);
    } else {
        ui->lcdP3->setStyleSheet(statusColors[g3.status]);
    }
    //update channel 1
    ui->diV1->setValue(ch1.readV/1000);
    //ui->diV1->setStyleSheet(statusColors[(int)(ch1.readV/1000+0.5)]);
    ui->thI1->setValue(ch1.readI*1000);
    ui->lcdV1->display(QString::number(ch1.readV/1000));
    ui->lcdI1->display(QString::number(ch1.readI*1000));
    //update channel 2
    ui->diV2->setValue(ch2.readV/1000);
    ui->thI2->setValue(ch2.readI*1000);
    ui->lcdV2->display(QString::number(ch2.readV/1000));
    ui->lcdI2->display(QString::number(ch2.readI*1000));

}





void MainWindow::setupDial(QwtDial *dial)
{
    QwtRoundScaleDraw *scaleDraw = new QwtRoundScaleDraw();
    scaleDraw->setSpacing( 8 );
    scaleDraw->enableComponent( QwtAbstractScaleDraw::Backbone, false );
    scaleDraw->setTickLength( QwtScaleDiv::MinorTick, 0 );
    scaleDraw->setTickLength( QwtScaleDiv::MediumTick, 4 );
    scaleDraw->setTickLength( QwtScaleDiv::MajorTick, 8 );
    dial->setScaleDraw( scaleDraw );

    dial->setWrapping( false );
    dial->setReadOnly( true );

    dial->setOrigin( 135.0 );
    dial->setScaleArc( 0.0, 270.0 );

    dial->setScaleStepSize( 0.2 );
    dial->setScale( 0.0, 2.0 );
    dial->scaleDraw()->setPenWidth( 2 );

    QwtDialSimpleNeedle *needle = new QwtDialSimpleNeedle(
        QwtDialSimpleNeedle::Arrow, true, Qt::red,
        QColor( Qt::gray ).light( 130 ) );
    dial->setNeedle( needle );

}


void MainWindow::setupTher(QwtThermo *ther)
{
    ther->setScale(0,1);
    ther->setValue(0.6);
    ther->setFillBrush(Qt::green);
    ther->setAlarmEnabled(true);
    ther->setAlarmBrush(Qt::red);
    ther->setAlarmLevel(0.5);
    ther->setOrientation(Qt::Horizontal);
    ther->setScalePosition(QwtThermo::LeadingScale);
    ther->setSpacing(0);
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About MCP Protector"),
                       tr("The <b>MCP Protector</b> ***"));
}

void MainWindow::on_spinBoxV1_valueChanged(double arg1)
{
    ch1.setV = arg1*1000;
    qDebug()<<"Set Channel-1 Voltage to:"<<ch1.setV;
}

void MainWindow::on_spinBoxI1_valueChanged(double arg1)
{
    ch1.setI = arg1/1000;
    qDebug()<<"Set Channel-1 Current to:"<<ch1.setI;
}

void MainWindow::on_spinBoxV2_valueChanged(double arg1)
{
    ch2.setV = arg1*1000;
    qDebug()<<"Set Channel-2 Voltage to:"<<ch2.setV;
}

void MainWindow::on_spinBoxI2_valueChanged(double arg1)
{
    ch2.setI = arg1/1000;
    qDebug()<<"Set Channel-2 Current to:"<<ch2.setI;
}


void MainWindow::setToZero()
{
    if(!sys.safe) {
        ui->spinBoxV1->setValue(ch1.setV);
        ui->spinBoxV2->setValue(ch2.setV);
        ui->spinBoxI1->setValue(ch1.setI);
        ui->spinBoxI2->setValue(ch2.setI);
        //warning
        this->setStyleSheet("background-color: red;");
        turnRed = true;
    } else {
        if( turnRed ) {
            this->setStyleSheet(" background-color: rgb(240,240,240) ");
        }
    }
}

