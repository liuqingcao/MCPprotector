#include "g1com.h"
#include "global.h"
#include <QDebug>


extern struct Settings G1currentSettings;
extern struct Gauge g1;


G1Com::G1Com()
{

}

G1Com::~G1Com()
{

}

void G1Com::run()
{
    checkChar = "";

    serial = new QSerialPort(this);
    serial->setPortName(G1currentSettings.name);
    serial->setBaudRate(G1currentSettings.baudRate);
    serial->setDataBits(G1currentSettings.dataBits);
    serial->setParity(G1currentSettings.parity);
    serial->setStopBits(G1currentSettings.stopBits);
    serial->setFlowControl(G1currentSettings.flowControl);
    if (serial->open(QIODevice::ReadWrite)) {
        serial->write("\x03");
        serial->clear();
        serial->write("COM,2\n");
        serial->waitForBytesWritten(100);
        serial->waitForReadyRead(500);
        data = serial->readAll();
        qDebug()<<"G1 Receive:"<<data;
        if (QString::fromUtf8(data.data()) == checkChar) {
            G1currentSettings.status = true;
            qDebug()<<"G1: Connected!";
            serial->write("\x03");
            serial->clear();
            serial->write("COM,1\n");
            //find the begining
            int n = 1;
            while(n)
            {
                serial->waitForReadyRead(1);
                QByteArray test = serial->readAll();
                if (QString::fromUtf8(test.data()) == "\n") {
                    n=0;
                }
            }

            while(1)
            {
                if(serial->bytesAvailable() == 43) {
                    data = serial->readAll();
                    g1.status = data.mid(3,1).toInt();
                    g1.value = data.mid(5,11).toDouble();
                    //qDebug()<<"G1:"<<data<<endl;
                    //qDebug()<<data.mid(3,1)<<data.mid(5,11)<<endl;
                    //qDebug()<<g1.status<<g1.value;
                } else {
                    serial->waitForReadyRead(10);
                }
            }
        } else {
            G1currentSettings.status = false;
            qDebug()<<"G1: Wrong connection!";
            serial->close();
            emit finished();
        }
    } else {
        qDebug()<<"HV: Connection failed!";
    }
}

void G1Com::stop()
{
    qDebug()<<"G1 shut down!\n";
    if (serial->isOpen()) {
        serial->write("\x03");
        serial->clear();
        serial->close();
    }
    emit finished();
}
