#-------------------------------------------------
#
# Project created by QtCreator 2015-02-07T17:19:42
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pressureWatcher
TEMPLATE = app
RC_FILE = myapp.rc

#CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT


SOURCES += main.cpp\
        mainwindow.cpp \
    settingsdialog.cpp \
    thresholddialog.cpp \
    warmupdialog.cpp \
    hvcom.cpp \
    g1com.cpp \
    g2com.cpp \
    global.cpp \
    warmupthread.cpp

HEADERS  += mainwindow.h \
    settingsdialog.h \
    thresholddialog.h \
    warmupdialog.h \
    hvcom.h \
    global.h \
    global.h \
    hvcom.h \
    g1com.h \
    g2com.h \
    warmupthread.h

FORMS    += mainwindow.ui \
    settingsdialog.ui \
    thresholddialog.ui \
    warmupdialog.ui



 INCLUDEPATH += C:\qwt-6.1.2\include

 Debug:LIBS += \
            -LC:/qwt-6.1.2/lib \
            -lqwtd
 Release:LIBS += \
            -LC:/qwt-6.1.2/lib \
            -lqwt

RESOURCES += \
    resources.qrc
