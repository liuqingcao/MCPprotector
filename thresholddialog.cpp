#include "thresholddialog.h"
#include "ui_thresholddialog.h"

#include <QDebug>
#include "global.h"
#include <iostream>

extern struct System sys;

thresholdDialog::thresholdDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::thresholdDialog)
{
    ui->setupUi(this);
    ui->spinBox->setRange(0,100);
    ui->spinBox->setValue(sys.threshold*1e5);
    ui->spinBox->setSingleStep(10);
    ui->spinBox->setRange(1e-2,100);
    ui->spinBox->setSingleStep(0.1);
}

thresholdDialog::~thresholdDialog()
{
    delete ui;
}



void thresholdDialog::on_spinBox_valueChanged(double arg1)
{
    sys.threshold = arg1*1e-5;
    qDebug()<<"Set threshold to:"<<sys.threshold;
}

