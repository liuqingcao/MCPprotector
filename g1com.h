#ifndef G1COM_H
#define G1COM_H

#include <QObject>
#include <QtSerialPort/QSerialPort>

class G1Com : public QObject {
    Q_OBJECT

public:
    G1Com();
    ~G1Com();


public slots:
    void run();
    void stop();
signals:
    void finished();
public:

private:
    QSerialPort *serial;
    QByteArray data;
    QString checkChar;



};

#endif // G1COM_H
