#ifndef GLOBAL
#define GLOBAL

#include <QtSerialPort/QSerialPort>

struct Settings {
    bool status;
    QString name;
    qint32 baudRate;
    QString stringBaudRate;
    QSerialPort::DataBits dataBits;
    QString stringDataBits;
    QSerialPort::Parity parity;
    QString stringParity;
    QSerialPort::StopBits stopBits;
    QString stringStopBits;
    QSerialPort::FlowControl flowControl;
    QString stringFlowControl;
};

//extern struct Settings HVcurrentSettings;
//extern struct Settings G1currentSettings;
//extern struct Settings G2currentSettings;

struct HVinfo {
    double readV;
    double oldV;
    double setV;
    double readI;
    double oldI;
    double setI;
};

//extern struct HVinfo ch1;
//extern struct HVinfo ch2;


struct Gauge {
    int status;
    double value;
};

//extern struct Gauge g1;
//extern struct Gauge g2;
//extern struct Gauge g3;


struct System {
    double threshold;
    bool warmup;
    bool warmupRunning;
    int warmupSpeed;
    int warmupCh1;
    int warmupCh2;
    bool safe;
    bool first;

};


void initial();


#endif // GLOBAL

