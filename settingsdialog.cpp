/****************************************************************************
**
** Copyright (C) 2012 Denis Shienkov <denis.shienkov@gmail.com>
** Copyright (C) 2012 Laszlo Papp <lpapp@kde.org>
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtSerialPort module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include <QtSerialPort/QSerialPortInfo>
#include <QIntValidator>
#include <QLineEdit>
#include <QDebug>
#include <QThread>
#include "global.h"
#include <QDebug>
#include <windows.h>

extern struct Settings HVcurrentSettings;
extern struct Settings G1currentSettings;
extern struct Settings G2currentSettings;



QT_USE_NAMESPACE

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{

    ui->setupUi(this);

    connect(ui->applyButton, SIGNAL(clicked()),
            this, SLOT(apply()));
    //HV
    connect(ui->HVserialPortInfoListBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(HVshowPortInfo(int)));
    connect(ui->HVbaudRateBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(HVcheckCustomBaudRatePolicy(int)));
    //Gauage-1
    connect(ui->G1serialPortInfoListBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(G1showPortInfo(int)));
    connect(ui->G1baudRateBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(G1checkCustomBaudRatePolicy(int)));
    //Gauage-2
    connect(ui->G2serialPortInfoListBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(G2showPortInfo(int)));
    connect(ui->G2baudRateBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(G2checkCustomBaudRatePolicy(int)));


    fillPortsParameters();
    fillPortsInfo();
    ui->HVserialPortInfoListBox->setCurrentIndex(2);
    ui->G1serialPortInfoListBox->setCurrentIndex(0);
    ui->G2serialPortInfoListBox->setCurrentIndex(1);
    updateSettings();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}


void SettingsDialog::HVshowPortInfo(int idx)
{
    if (idx != -1) {
        QStringList list = ui->HVserialPortInfoListBox->itemData(idx).toStringList();
        ui->HVdescriptionLabel->setText(tr("Description: %1").arg(list.at(1)));
        ui->HVmanufacturerLabel->setText(tr("Manufacturer: %1").arg(list.at(2)));
        ui->HVlocationLabel->setText(tr("Location: %1").arg(list.at(3)));
        ui->HVvidLabel->setText(tr("Vendor Identifier: %1").arg(list.at(4)));
        ui->HVpidLabel->setText(tr("Product Identifier: %1").arg(list.at(5)));
    }
}

void SettingsDialog::G1showPortInfo(int idx)
{
    if (idx != -1) {
        QStringList list = ui->G1serialPortInfoListBox->itemData(idx).toStringList();
        ui->G1descriptionLabel->setText(tr("Description: %1").arg(list.at(1)));
        ui->G1manufacturerLabel->setText(tr("Manufacturer: %1").arg(list.at(2)));
        ui->G1locationLabel->setText(tr("Location: %1").arg(list.at(3)));
        ui->G1vidLabel->setText(tr("Vendor Identifier: %1").arg(list.at(4)));
        ui->G1pidLabel->setText(tr("Product Identifier: %1").arg(list.at(5)));
    }
}

void SettingsDialog::G2showPortInfo(int idx)
{
    if (idx != -1) {
        QStringList list = ui->G2serialPortInfoListBox->itemData(idx).toStringList();
        ui->G2descriptionLabel->setText(tr("Description: %1").arg(list.at(1)));
        ui->G2manufacturerLabel->setText(tr("Manufacturer: %1").arg(list.at(2)));
        ui->G2locationLabel->setText(tr("Location: %1").arg(list.at(3)));
        ui->G2vidLabel->setText(tr("Vendor Identifier: %1").arg(list.at(4)));
        ui->G2pidLabel->setText(tr("Product Identifier: %1").arg(list.at(5)));
    }
}



void SettingsDialog::apply()
{
    closeSerialPort();
    Sleep(1000);
    //the settings
    updateSettings();
    //start HV
    QThread* threadHV = new QThread;
    hvCom* HVworker = new hvCom();
    HVworker->moveToThread(threadHV);
    connect(threadHV, SIGNAL(started()), HVworker, SLOT(run()));
    connect(HVworker, SIGNAL(finished()), threadHV, SLOT(quit()));
    connect(HVworker, SIGNAL(finished()), HVworker, SLOT(deleteLater()));
    connect(threadHV, SIGNAL(finished()), threadHV, SLOT(deleteLater()));
    threadHV->start();
    connect(this, SIGNAL(stopHV()), HVworker, SLOT(stop()));
    //start G1
    QThread* threadG1 = new QThread;
    G1Com* G1worker = new G1Com();
    G1worker->moveToThread(threadG1);
    connect(threadG1, SIGNAL(started()), G1worker, SLOT(run()));
    connect(G1worker, SIGNAL(finished()), threadG1, SLOT(quit()));
    connect(G1worker, SIGNAL(finished()), G1worker, SLOT(deleteLater()));
    connect(threadG1, SIGNAL(finished()), threadG1, SLOT(deleteLater()));
    threadG1->start();
    connect(this, SIGNAL(stopG1()), G1worker, SLOT(stop()));
    //start G2
    QThread* threadG2 = new QThread;
    G2Com* G2worker = new G2Com();
    G2worker->moveToThread(threadG2);
    connect(threadG2, SIGNAL(started()), G2worker, SLOT(run()));
    connect(G2worker, SIGNAL(finished()), threadG2, SLOT(quit()));
    connect(G2worker, SIGNAL(finished()), G2worker, SLOT(deleteLater()));
    connect(threadG2, SIGNAL(finished()), threadG2, SLOT(deleteLater()));
    threadG2->start();
    connect(this, SIGNAL(stopG2()), G2worker, SLOT(stop()));
    //
    Sleep(1000);
    if (HVcurrentSettings.status &
        G1currentSettings.status &
        G2currentSettings.status) {
        hide();
    }
}

void SettingsDialog::closeSerialPort()
{
    qDebug()<<"Stop the Serial communication!";
    if (HVcurrentSettings.status) {
        HVcurrentSettings.status = false;
        emit stopHV();
    }
    if (G1currentSettings.status) {
        G1currentSettings.status = false;
        emit stopG1();
    }
    if (G2currentSettings.status) {
        G2currentSettings.status = false;
        emit stopG2();
    }
}




void SettingsDialog::HVcheckCustomBaudRatePolicy(int idx)
{
    bool isCustomBaudRate = !ui->HVbaudRateBox->itemData(idx).isValid();
    ui->HVbaudRateBox->setEditable(isCustomBaudRate);
    if (isCustomBaudRate) {
        ui->HVbaudRateBox->clearEditText();
        QLineEdit *edit = ui->HVbaudRateBox->lineEdit();
        edit->setValidator(intValidator);
    }
}

void SettingsDialog::G1checkCustomBaudRatePolicy(int idx)
{
    bool isCustomBaudRate = !ui->G1baudRateBox->itemData(idx).isValid();
    ui->G1baudRateBox->setEditable(isCustomBaudRate);
    if (isCustomBaudRate) {
        ui->G1baudRateBox->clearEditText();
        QLineEdit *edit = ui->G1baudRateBox->lineEdit();
        edit->setValidator(intValidator);
    }
}

void SettingsDialog::G2checkCustomBaudRatePolicy(int idx)
{
    bool isCustomBaudRate = !ui->G2baudRateBox->itemData(idx).isValid();
    ui->G2baudRateBox->setEditable(isCustomBaudRate);
    if (isCustomBaudRate) {
        ui->G2baudRateBox->clearEditText();
        QLineEdit *edit = ui->G2baudRateBox->lineEdit();
        edit->setValidator(intValidator);
    }
}


void SettingsDialog::fillPortsParameters()
{
    //HV
    ui->HVbaudRateBox->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);
    ui->HVbaudRateBox->addItem(QStringLiteral("19200"), QSerialPort::Baud19200);
    ui->HVbaudRateBox->addItem(QStringLiteral("38400"), QSerialPort::Baud38400);
    ui->HVbaudRateBox->addItem(QStringLiteral("115200"), QSerialPort::Baud115200);
    ui->HVbaudRateBox->addItem(QStringLiteral("Custom"));

    ui->HVdataBitsBox->addItem(QStringLiteral("5"), QSerialPort::Data5);
    ui->HVdataBitsBox->addItem(QStringLiteral("6"), QSerialPort::Data6);
    ui->HVdataBitsBox->addItem(QStringLiteral("7"), QSerialPort::Data7);
    ui->HVdataBitsBox->addItem(QStringLiteral("8"), QSerialPort::Data8);
    ui->HVdataBitsBox->setCurrentIndex(3);

    ui->HVparityBox->addItem(QStringLiteral("None"), QSerialPort::NoParity);
    ui->HVparityBox->addItem(QStringLiteral("Even"), QSerialPort::EvenParity);
    ui->HVparityBox->addItem(QStringLiteral("Odd"), QSerialPort::OddParity);
    ui->HVparityBox->addItem(QStringLiteral("Mark"), QSerialPort::MarkParity);
    ui->HVparityBox->addItem(QStringLiteral("Space"), QSerialPort::SpaceParity);

    ui->HVstopBitsBox->addItem(QStringLiteral("1"), QSerialPort::OneStop);
#ifdef Q_OS_WIN
    ui->HVstopBitsBox->addItem(QStringLiteral("1.5"), QSerialPort::OneAndHalfStop);
#endif
    ui->HVstopBitsBox->addItem(QStringLiteral("2"), QSerialPort::TwoStop);

    ui->HVflowControlBox->addItem(QStringLiteral("None"), QSerialPort::NoFlowControl);
    ui->HVflowControlBox->addItem(QStringLiteral("RTS/CTS"), QSerialPort::HardwareControl);
    ui->HVflowControlBox->addItem(QStringLiteral("XON/XOFF"), QSerialPort::SoftwareControl);

    //G1
    ui->G1baudRateBox->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);
    ui->G1baudRateBox->addItem(QStringLiteral("19200"), QSerialPort::Baud19200);
    ui->G1baudRateBox->addItem(QStringLiteral("38400"), QSerialPort::Baud38400);
    ui->G1baudRateBox->addItem(QStringLiteral("115200"), QSerialPort::Baud115200);
    ui->G1baudRateBox->addItem(QStringLiteral("Custom"));

    ui->G1dataBitsBox->addItem(QStringLiteral("5"), QSerialPort::Data5);
    ui->G1dataBitsBox->addItem(QStringLiteral("6"), QSerialPort::Data6);
    ui->G1dataBitsBox->addItem(QStringLiteral("7"), QSerialPort::Data7);
    ui->G1dataBitsBox->addItem(QStringLiteral("8"), QSerialPort::Data8);
    ui->G1dataBitsBox->setCurrentIndex(3);

    ui->G1parityBox->addItem(QStringLiteral("None"), QSerialPort::NoParity);
    ui->G1parityBox->addItem(QStringLiteral("Even"), QSerialPort::EvenParity);
    ui->G1parityBox->addItem(QStringLiteral("Odd"), QSerialPort::OddParity);
    ui->G1parityBox->addItem(QStringLiteral("Mark"), QSerialPort::MarkParity);
    ui->G1parityBox->addItem(QStringLiteral("Space"), QSerialPort::SpaceParity);

    ui->G1stopBitsBox->addItem(QStringLiteral("1"), QSerialPort::OneStop);
#ifdef Q_OS_WIN
    ui->G1stopBitsBox->addItem(QStringLiteral("1.5"), QSerialPort::OneAndHalfStop);
#endif
    ui->G1stopBitsBox->addItem(QStringLiteral("2"), QSerialPort::TwoStop);

    ui->G1flowControlBox->addItem(QStringLiteral("None"), QSerialPort::NoFlowControl);
    ui->G1flowControlBox->addItem(QStringLiteral("RTS/CTS"), QSerialPort::HardwareControl);
    ui->G1flowControlBox->addItem(QStringLiteral("XON/XOFF"), QSerialPort::SoftwareControl);

    //G2
    ui->G2baudRateBox->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);
    ui->G2baudRateBox->addItem(QStringLiteral("19200"), QSerialPort::Baud19200);
    ui->G2baudRateBox->addItem(QStringLiteral("38400"), QSerialPort::Baud38400);
    ui->G2baudRateBox->addItem(QStringLiteral("115200"), QSerialPort::Baud115200);
    ui->G2baudRateBox->addItem(QStringLiteral("Custom"));

    ui->G2dataBitsBox->addItem(QStringLiteral("5"), QSerialPort::Data5);
    ui->G2dataBitsBox->addItem(QStringLiteral("6"), QSerialPort::Data6);
    ui->G2dataBitsBox->addItem(QStringLiteral("7"), QSerialPort::Data7);
    ui->G2dataBitsBox->addItem(QStringLiteral("8"), QSerialPort::Data8);
    ui->G2dataBitsBox->setCurrentIndex(3);

    ui->G2parityBox->addItem(QStringLiteral("None"), QSerialPort::NoParity);
    ui->G2parityBox->addItem(QStringLiteral("Even"), QSerialPort::EvenParity);
    ui->G2parityBox->addItem(QStringLiteral("Odd"), QSerialPort::OddParity);
    ui->G2parityBox->addItem(QStringLiteral("Mark"), QSerialPort::MarkParity);
    ui->G2parityBox->addItem(QStringLiteral("Space"), QSerialPort::SpaceParity);

    ui->G2stopBitsBox->addItem(QStringLiteral("1"), QSerialPort::OneStop);
#ifdef Q_OS_WIN
    ui->G2stopBitsBox->addItem(QStringLiteral("1.5"), QSerialPort::OneAndHalfStop);
#endif
    ui->G2stopBitsBox->addItem(QStringLiteral("2"), QSerialPort::TwoStop);

    ui->G2flowControlBox->addItem(QStringLiteral("None"), QSerialPort::NoFlowControl);
    ui->G2flowControlBox->addItem(QStringLiteral("RTS/CTS"), QSerialPort::HardwareControl);
    ui->G2flowControlBox->addItem(QStringLiteral("XON/XOFF"), QSerialPort::SoftwareControl);

}

void SettingsDialog::fillPortsInfo()
{
    ui->HVserialPortInfoListBox->clear();
    ui->G1serialPortInfoListBox->clear();
    ui->G2serialPortInfoListBox->clear();

    static const QString blankString = QObject::tr("N/A");
    QString description;
    QString manufacturer;
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        QStringList list;
        description = info.description();
        manufacturer = info.manufacturer();
        list << info.portName()
             << (!description.isEmpty() ? description : blankString)
             << (!manufacturer.isEmpty() ? manufacturer : blankString)
             << info.systemLocation()
             << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : blankString)
             << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : blankString);

        ui->HVserialPortInfoListBox->addItem(list.first(), list);
        ui->G1serialPortInfoListBox->addItem(list.first(), list);
        ui->G2serialPortInfoListBox->addItem(list.first(), list);
    }
}

void SettingsDialog::updateSettings()
{
    //HV
    HVcurrentSettings.name = ui->HVserialPortInfoListBox->currentText();

    if (ui->HVbaudRateBox->currentIndex() == 4) {
        HVcurrentSettings.baudRate = ui->HVbaudRateBox->currentText().toInt();
    } else {
        HVcurrentSettings.baudRate = static_cast<QSerialPort::BaudRate>(
                    ui->HVbaudRateBox->itemData(ui->HVbaudRateBox->currentIndex()).toInt());
    }
    HVcurrentSettings.stringBaudRate = QString::number(HVcurrentSettings.baudRate);

    HVcurrentSettings.dataBits = static_cast<QSerialPort::DataBits>(
                ui->HVdataBitsBox->itemData(ui->HVdataBitsBox->currentIndex()).toInt());
    HVcurrentSettings.stringDataBits = ui->HVdataBitsBox->currentText();

    HVcurrentSettings.parity = static_cast<QSerialPort::Parity>(
                ui->HVparityBox->itemData(ui->HVparityBox->currentIndex()).toInt());
    HVcurrentSettings.stringParity = ui->HVparityBox->currentText();

    HVcurrentSettings.stopBits = static_cast<QSerialPort::StopBits>(
                ui->HVstopBitsBox->itemData(ui->HVstopBitsBox->currentIndex()).toInt());
    HVcurrentSettings.stringStopBits = ui->HVstopBitsBox->currentText();

    HVcurrentSettings.flowControl = static_cast<QSerialPort::FlowControl>(
                ui->HVflowControlBox->itemData(ui->HVflowControlBox->currentIndex()).toInt());
    HVcurrentSettings.stringFlowControl = ui->HVflowControlBox->currentText();

    //G1
    G1currentSettings.name = ui->G1serialPortInfoListBox->currentText();

    if (ui->G1baudRateBox->currentIndex() == 4) {
        G1currentSettings.baudRate = ui->G1baudRateBox->currentText().toInt();
    } else {
        G1currentSettings.baudRate = static_cast<QSerialPort::BaudRate>(
                    ui->G1baudRateBox->itemData(ui->G1baudRateBox->currentIndex()).toInt());
    }
    G1currentSettings.stringBaudRate = QString::number(G1currentSettings.baudRate);

    G1currentSettings.dataBits = static_cast<QSerialPort::DataBits>(
                ui->G1dataBitsBox->itemData(ui->G1dataBitsBox->currentIndex()).toInt());
    G1currentSettings.stringDataBits = ui->G1dataBitsBox->currentText();

    G1currentSettings.parity = static_cast<QSerialPort::Parity>(
                ui->G1parityBox->itemData(ui->G1parityBox->currentIndex()).toInt());
    G1currentSettings.stringParity = ui->G1parityBox->currentText();

    G1currentSettings.stopBits = static_cast<QSerialPort::StopBits>(
                ui->G1stopBitsBox->itemData(ui->G1stopBitsBox->currentIndex()).toInt());
    G1currentSettings.stringStopBits = ui->G1stopBitsBox->currentText();

    G1currentSettings.flowControl = static_cast<QSerialPort::FlowControl>(
                ui->G1flowControlBox->itemData(ui->G1flowControlBox->currentIndex()).toInt());
    G1currentSettings.stringFlowControl = ui->G1flowControlBox->currentText();


    //G2
    G2currentSettings.name = ui->G2serialPortInfoListBox->currentText();

    if (ui->G2baudRateBox->currentIndex() == 4) {
        G2currentSettings.baudRate = ui->G2baudRateBox->currentText().toInt();
    } else {
        G2currentSettings.baudRate = static_cast<QSerialPort::BaudRate>(
                    ui->G2baudRateBox->itemData(ui->G2baudRateBox->currentIndex()).toInt());
    }
    G2currentSettings.stringBaudRate = QString::number(G2currentSettings.baudRate);

    G2currentSettings.dataBits = static_cast<QSerialPort::DataBits>(
                ui->G2dataBitsBox->itemData(ui->G2dataBitsBox->currentIndex()).toInt());
    G2currentSettings.stringDataBits = ui->G2dataBitsBox->currentText();

    G2currentSettings.parity = static_cast<QSerialPort::Parity>(
                ui->G2parityBox->itemData(ui->G2parityBox->currentIndex()).toInt());
    G2currentSettings.stringParity = ui->G2parityBox->currentText();

    G2currentSettings.stopBits = static_cast<QSerialPort::StopBits>(
                ui->G2stopBitsBox->itemData(ui->G2stopBitsBox->currentIndex()).toInt());
    G2currentSettings.stringStopBits = ui->G2stopBitsBox->currentText();

    G2currentSettings.flowControl = static_cast<QSerialPort::FlowControl>(
                ui->G2flowControlBox->itemData(ui->G2flowControlBox->currentIndex()).toInt());
    G2currentSettings.stringFlowControl = ui->G2flowControlBox->currentText();


}
