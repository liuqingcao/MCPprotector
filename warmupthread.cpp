#include "warmupthread.h"
#include "global.h"
#include <windows.h>
#include <QDebug>

extern struct System sys;
extern struct HVinfo ch1;
extern struct HVinfo ch2;

warmupThread::warmupThread()
{

}

warmupThread::~warmupThread()
{

}



void warmupThread::turnOn()
{

    int steps = int(sys.warmupCh1/sys.warmupSpeed);
    emit processRange(0,steps);
    qDebug()<<"Turn on MCP, speed"<<sys.warmupSpeed<<"V/s, need"<<steps<<"seconds";

    int i = 0;
    sys.warmupRunning = true;
    while(sys.warmupRunning & (i <= steps)) {
        ch1.setV = i*sys.warmupSpeed;
        ch2.setV = i*sys.warmupSpeed*sys.warmupCh2/sys.warmupCh1;
        emit process(i);
        Sleep(1000);
        i++;
    }
    if (i > steps) {
        sys.warmup = true;
        sys.first = true;
    } else {
        sys.warmup = false;
    }
}


void warmupThread::shutDown()
{

    int steps = int((ch1.readV + 0.001)/sys.warmupSpeed);
    emit processRange(0,steps);
    qDebug()<<"Turn off MCP, speed"<<sys.warmupSpeed<<"V/s, need"<<steps<<"seconds";

    int i = steps;
    sys.warmupRunning = true;
    while(sys.warmupRunning & (i >= 0)) {
        ch1.setV = i*sys.warmupSpeed;
        ch2.setV = i*sys.warmupSpeed*ch2.readV/(ch1.readV + 0.001);
        emit process(i);
        Sleep(1000);
        i--;
    }
    if (i < 0) {
        sys.warmup = false;
        sys.first = true;
    } else {
        sys.warmup = true;
    }
}



void warmupThread::destoryThread()
{
    emit finished();
}
