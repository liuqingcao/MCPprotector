#ifndef WARMUPTHREAD_H
#define WARMUPTHREAD_H

#include <QObject>

class warmupThread: public QObject {
    Q_OBJECT

public:
    warmupThread();
    ~warmupThread();


public slots:
    void turnOn();
    void shutDown();
    void destoryThread();

signals:
    void finished();
    void process(int);
    void processRange(int,int);

private:
    bool running;

};










#endif // WARMUPTHREAD_H
